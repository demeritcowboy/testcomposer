<?php
namespace AbcDef\Banana;

class Eater {
  public function eat() {
    return "Banana eaten.";
  }
}
